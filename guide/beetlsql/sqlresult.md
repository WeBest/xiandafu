### 直接使用SQLResult  {docsify-ignore}

有时候，也许你只需要SQL及其参数列表，然后传给你自己的dao工具类，这时候你需要SQLResult，它包含了你需要的sql，和sql参数。 SQLManager 有如下方法，你需要传入sqlid，和参数即可

```java
public SQLResult getSQLResult(String id, Map<String, Object> paras)
```

paras 是一个map，如果你只有一个pojo作为参数，你可以使用“_root” 作为key，这样sql模版找不到名称对应的属性值的时候，会寻找_root 对象，如果存在，则取其同名属性。

SQLResult 如下：

```java
public class SQLResult {
	public String jdbcSql;
	public List<SQLParameter> jdbcPara;
	public Object[] toObjectArray(){}
}
```

jdbcSql是渲染过后的sql，jdbcPara 是对应的参数描述，toObjectArray 是sql对应的参数值。

SQLParameter 用来描述参数，主要包含了

* value: 参数值
* expression ，参数对应的表达式,如下sql

~~~sql
select * from user where id = #id#
~~~

则expression 就是字符串id

* type，expression 类型，因为sql里有可能是一个复杂的表达式，因此type有如下值

  NAME_GENEARL:简单的表达式，如id

  NAME_EXPRESSION：复杂表达式，比如函数调用，逻辑运算表达式

对于开发者来说，只需要关心sql对应的参数值即可，因此可以调用toObjectArray得到。
