### Interceptor功能  {docsify-ignore}

BeetlSql可以在执行sql前后执行一系列的Intercetor，从而有机会执行各种扩展和监控，这比已知的通过数据库连接池做Interceptor更加容易。如下Interceptor都是有可能的

-   监控sql执行较长时间语句，打印并收集。TimeStatInterceptor 类完成
-   对每一条sql语句执行后输出其sql和参数，也可以根据条件只输出特定sql集合的sql。便于用户调试。DebugInterceptor完成
-   汇总sql执行情况（未完成，需要集成第三方sql分析工具）
-   跟踪执行的sql和响应的参数

> DebugInterceptor 类是常用类，会将sql和参数打印到控制台，如果你想集合log4j等工具，你需要继承DebugInterceptor，实现println方法，使用你的log框架打印

你也可以自行扩展Interceptor类，来完成特定需求。 如下，在执行数据库操作前会执行before，通过ctx可以获取执行的上下文参数，数据库成功执行后，会执行after方法

```java
public interface Interceptor {
	public void before(InterceptorContext ctx);
	public void after(InterceptorContext ctx);
}
```

InterceptorContext 如下，包含了sqlId，实际得sql，和实际得参数, 也包括执行结果result。对于查询，执行结果是查询返回的结果集条数，对于更新，返回的是成功条数，如果是批量更新，则是一个数组。可以参考源码DebugInterceptor

```java
public class InterceptorContext {
	private String sqlId;
	private String sql;
	private  List<SQLParameter> paras;
	private boolean isUpdate = false ;
	private Object result ;
	private Map<String,Object> env  = null;
}
```



对于批处理更新，使用BatchUpdateInterceptorContext 代替InterceptorContext，paras参数为空，增加了batchParas，保留了所有的参数，定义如下

```java
public class BatchUpdateInterceptorContext  extends  InterceptorContext{
    protected List<List<SQLParameter>> batchParas;
    public BatchUpdateInterceptorContext(String sqlId, String sql, List<List<SQLParameter>> batchParas ) {
        super(sqlId, sql, Collections.emptyList(), Collections.emptyMap(), true);
        this.batchParas = batchParas;
    }
}
```

