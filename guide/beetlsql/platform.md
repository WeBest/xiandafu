### 跨数据库平台  {docsify-ignore}

如前所述，BeetlSql 可以通过sql文件的管理和搜索来支持跨数据库开发，如前所述，先搜索特定数据库，然后再查找common。另外BeetlSql也提供了一些夸数据库解决方案

-   DbStyle 描述了数据库特性，注入insert语句，翻页语句都通过其子类完成，用户无需操心
-   提供一些默认的函数扩展，代替各个数据库的函数，如时间和时间操作函数date等
-   MySqlStyle mysql 数据库支持
-   OracleStyle oralce支持
-   PostgresStyle postgresql数据库支持
-   其他还有SQLServer,H2,SQLLite ，DB2数据库支持
