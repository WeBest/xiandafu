### 复合主键 {docsify-ignore}

beetlsql 支持复合主键，无需像其他dao工具那样创建一个特别的主键对象，主键对象就是实体对象本身

```sql
CREATE TABLE `party` (
  `id1` int(11) NOT NULL,
  `id2` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id1`,`id2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
```

Party代码如下

```java
public class Party  {
    @AssignID
	private Integer id1 ;
	@AssignID
	private Integer id2 ;
	private String name ;
	//忽略其他 getter setter方法
}
```

根据主键获取Party

```java
Party key = new Party();
key.setId1(1);
key.setId2(2);
Party party = sql.unique(Party.class, key);
```
