# coding=utf-8

# merge all md by _sidebar.md
import sys

import os
import os.path
import time
time1=time.time()

def mergeTxt(root,list,outfile):
    if(os.path.exists(outfile)):
        os.remove(outfile)
    k = open(outfile, 'a+',encoding='utf-8')

    for filePath in list:
        txtPath ="."+filePath+".md"
        f = open(txtPath)
        k.write(f.read()+"\n")
    k.close()

    print("finished")

def getIndex(indexFile):
    k = open(indexFile, 'r', encoding='utf-8')
    str = k.read();
    list = []

    index = str.find('(')
    while index != -1:
        end = str.find(')', index)
        list.append(str[index + 1:end])
        index = str.find('(', end + 1)
    return list;

if __name__ == '__main__':
    indexFile = "./beetlsql/_sidebar.md"
    filepath="./beetl"
    outfile="./beetlsql/all.md"
    list = getIndex(indexFile);
    mergeTxt(filepath,list,outfile)
    time2 = time.time()
    print(u'总共耗时：' + str(time2 - time1) + 's')

