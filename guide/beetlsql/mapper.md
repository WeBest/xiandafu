### 使用Mapper {docsify-ignore}

SQLManager 提供了所有需要知道的API，但通过sqlid来访问sql有时候还是很麻烦，因为需要手敲字符串，另外参数不是map就是para，对代码理解没有好处，BeetlSql支持Mapper，将sql文件映射到一个interface接口。接口的方法名与sql文件desqlId一一对应。

接口必须实现BaseMapper接口（后面可以自定义一个Base接口），它提供内置的CRUID方法，如insert,unique,template,templateOne ,updateById等


BaseMapper 具备数据库常见的操作，接口只需要定义额外的方法与sqlId同名即可。

~~~java
public interface UserDao extends BaseMapper<User> {
	List<User> select(String name);
}
~~~

如上select将会对应如下md文件

~~~markdown
select
===

	select * from user where name = #name#
	
~~~

如果你使用JDK8，不必为参数提供名称，自动对应。但必须保证java编译的时候开启-parameters选项。如果使用JDK8以下的版本，则可以使用@Param注解()

~~~java

List<User> select(@Param("name") String name);
~~~

BeetlSql的mapper方法总会根据调用方法名字，返回值，以及参数映射到SQLManager相应的查询接口，比如返回类型是List，意味着发起SQLManager.select 查询，如果返回是一个Map或者Pojo，则发起一次selectSingle查询，如果返回定义为List，则表示查询实体，如果定义为List<Long> ，则对应的查询结果映射为Long

定义好接口后，可以通过SQLManager.getMapper 来获取一个Dao真正的实现

~~~java
UserDao dao = sqlManager.getMapper(UserDao.class);
~~~

如果你使用Spring或者SpringBoot，可以参考Spring集成一章，了解如何自动注入Mapper

> Mapper 对应的sql文件默认根据实体来确定，如实体是UserInfo对象，则对应的sql文件是userInfo.md(sql),可以通过@SqlResource 注解来指定Mapper对应的sql文件。比如

~~~java
@SqlResource("core.user")
public interface UserCoreDao extends BaseMapper<User> {
	List<User> select(String name);
}
@SqlResource("console.user")
public interface UserConsoleDao extends BaseMapper<User> {
	List<User> select(String name);
}
~~~

> 这样，这俩个mapper分别访问sql/core/user.md 和 sql/console/user.md

#### 内置CRUD

BaseMapper包含了内置的常用查询，如下

~~~java
public interface BaseMapper<T> {

	/**
	 * 通用插入，插入一个实体对象到数据库，所以字段将参与操作，除非你使用ColumnIgnore注解
	 * @param entity
	 */
	void insert(T entity);
	/**
	 * （数据库表有自增主键调用此方法）如果实体对应的有自增主键，插入一个实体到数据库，设置assignKey为true的时候，将会获取此主键
	 * @param entity
	 * @param autDbAssignKey 是否获取自增主键
	 */
	void insert(T entity,boolean autDbAssignKey);
	/**
	 * 插入实体到数据库，对于null值不做处理
	 * @param entity
	 */
	void insertTemplate(T entity);
	/**
	 * 如果实体对应的有自增主键，插入实体到数据库，对于null值不做处理,设置assignKey为true的时候，将会获取此主键
	 * @param entity
	 * @param autDbAssignKey
	 */
	void insertTemplate(T entity,boolean autDbAssignKey);
	/**
	 * 批量插入实体。此方法不会获取自增主键的值，如果需要，建议不适用批量插入，适用
	 * <pre>
	 * insert(T entity,true);
	 * </pre>
	 * @param list
	 */
	void insertBatch(List<T> list);
	/**
	 * （数据库表有自增主键调用此方法）如果实体对应的有自增主键，插入实体到数据库，自增主键值放到keyHolder里处理
	 * @param entity
	 * @return
	 */
	KeyHolder insertReturnKey(T entity);
	
	/**
	 * 根据主键更新对象，所以属性都参与更新。也可以使用主键ColumnIgnore来控制更新的时候忽略此字段
	 * @param entity
	 * @return
	 */
	int updateById(T entity);
	/**
	 * 根据主键更新对象，只有不为null的属性参与更新
	 * @param entity
	 * @return
	 */
	int updateTemplateById(T entity);
	
	/**
	 * 根据主键删除对象，如果对象是复合主键，传入对象本生即可
	 * @param key
	 * @return
	 */
	int deleteById(Object key);
	
	/**
	 * 根据主键获取对象，如果对象不存在，则会抛出一个Runtime异常
	 * @param key
	 * @return
	 */
	T unique(Object key);
	/**
	 * 根据主键获取对象，如果对象不存在，返回null
	 * @param key
	 * @return
	 */
	T single(Object key);
	
	
	/**
	 * 根据主键获取对象，如果在事物中执行会添加数据库行级锁(select * from table where id = ? for update)，如果对象不存在，返回null
	 * @param key
	 * @return
	 */
	T lock(Object key);
	
	/**
	 * 返回实体对应的所有数据库记录
	 * @return
	 */
	List<T> all();
	/**
	 * 返回实体对应的一个范围的记录
	 * @param start
	 * @param size
	 * @return
	 */
	List<T> all(int start,int size);
	/**
	 * 返回实体在数据库里的总数
	 * @return
	 */
	long allCount();
	
	/**
	 * 模板查询，返回符合模板得所有结果。beetlsql将取出非null值（日期类型排除在外），从数据库找出完全匹配的结果集
	 * @param entity
	 * @return
	 */
	List<T> template(T entity);

	
	/**
	 * 模板查询，返回一条结果,如果没有，返回null
	 * @param entity
	 * @return
	 */
	<T> T templateOne(T entity);

	List<T> template(T entity,int start,int size);
	/**
	 * 符合模板得个数
	 * @param entity
	 * @return
	 */
	long templateCount(T entity);
	
    /**
	 * 单表的基于模板查询的翻页
	 * @param query
	 * @return
	 */
	void templatePage(PageQuery<T> query);
	
	/**
	 * 执行一个jdbc sql模板查询
	 * @param sql
	 * @param args
	 * @return
	 */
	List<T> execute(String sql,Object... args);
	/**
	 * 执行一个更新的jdbc sql
	 * @param sql
	 * @param args
	 * @return
	 */
	int executeUpdate(String sql,Object... args );
	SQLManager getSQLManager();
	/**
	 * 返回一个Query对象
	 * @return
	 */
	Query<T> createQuery();
    
     /**
     * 返回一个LambdaQuery对象
     * @return
     */
    LambdaQuery<T> createLambdaQuery();

}
~~~

> 内置BaseMapper 可以定制，参考文档最后一节25.6，设置自己的BaseMapper

#### sqlId查询

对于sqlId 是查询语句，返回值可以是任何类型，Mapper将视图将查询结果映射到定义的类型上，如下是一些常见例子

~~~java
public interface UserDao extends BaseMapper<User> {
  // 使用"user.getCount"语句,无参数
  public int getCount();
  //使用"user.findById"语句，参数是id，返回User对象
  public User findById(@Param("id") Integer id);
  //使用"user.findById"语句，参数是id，返回User对象
  public List<User> findByName(@Param("name") String name);
  //使用user.findTop10OfIds语句, 查询结果映射为Long，比如“select id from user limit 10
  public List<Long> findTop10OfIds();
  //返回一个Map，不建议这么做，最好返回一个实体，或者实体+Map的混合模型(参考BeetlSql模型)
  public List<Map<String,Object> findUsers(@Param("name") String name,@Param("departmentId") departmentId)
  
}
~~~

>  对于jdk8以上的，不必使用@Param注解。但必须保证java编译代码的时候开启-parameters选项

Mapper 查询有一个例外，如果第一个参数是一个JavaBean(即非java内置对象)，则默认为是_root对象，因此如下俩个接口定义是等价的

~~~java
public List query(User template);
public List query2(@Param("_root") User template)

~~~
这俩个查询方法都将template视为一个\_root对象，sql语句可以直接使用引用其属性。同样第一参数类型是Map的，BeetlSql也视为\_root对象。

如果需要查询指定范围内的结果集，可以使用@RowStart,@RowSize , 将指示Mapper发起一次范围查询(参考3.2.3)
~~~java
pulic List<User> selectRange(User data,Date maxTime,@RowStart int start，@RowSize int size)
~~~
如上查询语句，类似这样调用了SQLManager
~~~java
Map paras = new HashMap();
paras.put("_root",data);
paras.put("maxTime",maxTime);
List<User> list = sqlManager.select("user.selectRanage",User.class,paras,start,size);
~~~

#### PageQuery 查询

PageQuery查询类似上一节的sqlId查询，不同的是，需要提供PageQuery参数以让Mapper理解为PageQuery查询，如下俩个是等价的

~~~java
public void queryByCondtion(PageQuery query);
public PageQuery queryByCondtion(PageQuery query);
~~~
可以添加额外参数，但PageQuery 必须是第一个参数，如
~~~java
public void queryByCondtion(PageQuery query,Date maxTime);
~~~
这类似如下SQLManager调用
~~~java
query.setPara("maxTime",maxTime);
sqlManager.pageQuery("user.queryByCondtion",User.class,query)
~~~

也可以在方法中提供翻页参数来实现翻页查询，这时候返回值必须是PageQuery，如下
~~~java
public PageQuery queryByCondtion(int pageNumber,int pageSize,String name);
~~~
这种情况下，前俩个参数必须是int或者long类型

#### 更新语句
更新语句返回的结果可以是void，或者int，如果是批量更新，则可以返回int[]

~~~java
public int updaetUser(int id,String name);
public int updateUser(User user);
public int[] updateAll(List<User> list);
~~~

BeetlSql 通常根据返回值是int或者int[]  来判断是否是更新还是批量更新，如果没有返回值，会进一步判断第一个参数是否是集合或者数组，比如

~~~java
public void updateAllByUser(List<User> list);
public void updateAllByIds(List<Integer> list);
~~~

Beetl会假设前者是批量更新，而后者只是普通更新。建议还是不要使用void，而使用int或者int[]来帮助区分

#### 插入语句

插入语句同更新语句，唯一不同的是插入语句有时候需要获取自增序列值，这时候使用KeyHolder作为返回参数
~~~java
public KeyHolder insertSql(User user);
~~~

####6.6. 使用JDBC SQL
可以通过@Sql注解直接在java中使用较为简单的sql语句，如下
~~~java
@Sql(value=" update user set age = ? where id = ? ")
public void updateAge(int age,int id);
@Sql(value="select id from user where create_time<?")
public List<Long> selectIds(Date date)
~~~
此时方法参数与"?" 一一对应
也可以使用@Sql翻页，这要求方法参数前俩个必须是int或者long,返回结果使用PageQuery定义
~~~java
@Sql(value="select * from user where create_time<?")
public PageQuery selectUser(int pageNumber,int pageSize,Date date)
~~~

#### Mapper中的注解

从上面我们已经了解了@Param注解，用于申明参数名字，如果使用jdk8，且打开了编译选项parameters，则可以去掉@Param注解
@RowStart和 @RowSize，用于查询中的范围查询。
@Sql 注解则用于在java中构造一个简短的jdbc sql语句。

@SqlStatment 注解可以对接口参数进一步说明，他有如下属性
* type,用于说明sqlId是何种类型的语句，默认为auto，BeetlSql将会根据sqlId对应的Sql语句判断是否是查询，还是修改语句等，通常是根据sql语句的第一个词来判断，如果是select，表示查询，如果是insert，表示新增，如果update，drop，则是更新。如果Sql模板语句第一个词不包含这些，则需要用type做说明。如下是一个需要用到type的情况
~~~markdown
selectUsers
===
#use("otherSelect")# and status=1;
~~~
因为beetlsql无法根据第一个单词确定操作类型，因此必须使用type=SqlStatementType.SELECT，来说明。

* params ,可以不用在接口参数上使用@Param，而直接使用params 属性，如下是等价的

~~~java
@SqlStatement(params="name,age,_st,_sz")
public List<User> queryUser( String name, Integer age,int start, int size);

public List<User> queryUser( @Param(name) String name, @Param(age) @RowStart Integer age,int start, @RowSize int size);
~~~

\_st,_sz 同@RowStart和@RowSize 

#### 使用接口默认方法
如果你使用JDK8，则可以在mapper中添加默认方法，有利于重用
~~~java
public interface SysResourceDao extends BaseMapper<SysResource> {

    void page(PageQuery<SysResource> query);

    default List<SysResource> listChildren(Integer resourceId) {
        return createLambdaQuery()
                .andNotEq(SysResource::getStatus, 1)
                .andEq(SysResource::getPid, resourceId)
                .select();
    }
}
~~~
