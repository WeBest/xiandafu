### Debug功能  {docsify-ignore}

Debug 期望能在控制台或者日志系统输出执行的sql语句，参数，执行结果以及执行时间，可以采用系统内置的DebugInterceptor 来完成，在构造SQLManager的时候，传入即可

```java
SqlManager sqlManager = new SqlManager(source,mysql,loader,nc ,new Interceptor[]{new DebugInterceptor() });
```

或者通过spring，jfianl这样框架配置完成。使用后，执行beetlsql，会有类似输出

```sql
┏━━━━━ Debug [user.selectUserAndDepartment] ━━━
┣ SQL：	 select * from user where 1 = 1
┣ 参数：	 []
┣ 位置：	 org.beetl.sql.test.QuickTest.main(QuickTest.java:47)
┣ 时间：	 23ms
┣ 结果：	 [3]
┗━━━━━ Debug [user.selectUserAndDepartment] ━━━
```

beetlsql会分别输出 执行前的sql和参数，以及执行后的结果和耗费的时间。你可以参考DebugInterceptor 实现自己的调试输出

DebugInterceptor 还允许设置“位置”的实现，这是因为很多业务系统封装了Beetlsql，更希望"位置"打印的是业务系统，而非封装类的位置，具体可参考DebugInterceptor 源码



> 默认情况下，Spring 集成会自动集成DebugInterceptor，如果想取消，需要自己修改集成代码，不像SQLManager 传入DebugInterceptor
