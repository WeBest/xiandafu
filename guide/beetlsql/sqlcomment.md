### SQL 注释  {docsify-ignore}

对于采用Markdown方式，可以采用多种方式对sql注释。

-   采用sql 自己的注释符号，"-- " ,优点是适合java和数据库sql之间互相迁移，如

```sql
select * from user where
--  status 代表状态
statu = 1
```

-   采用beetl注释

```sql
select * from user where
@ /* 这些sql语句被注释掉
statu = 1
@ */
```

-   在sqlId 的=== 紧挨着的下一行 后面连续使用“*”作为sql整个语句注释

```markdown
selectByUser
===
* 这个sql语句用来查询用户的
* status =1 表示查找有效用户

select * from user where status = 1
```
