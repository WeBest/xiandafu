### Sharding-JDBC支持  {docsify-ignore}

开发者可以使用Sharding-JDBC来支持数据库分库分表，Sharding-JDBC 对外提供了一个封装好的数据源。只要将此DataSource当成普通数据源配置给BeetlSQL即可，唯一的问题是因为
Sharding-JDBC的数据源提供的MetaData功能较弱，因此构造BeetlSQL的ConnectionSource的时候还需要额外指定一个真正的数据源

```java

final DataSource one =  ....
finbal DataSource two = ....
final Map<String, DataSource> result = new HashMap<>(3, 1);
result.put("one",one);
result.put("two",two);

DataSource shardingDataSource = MasterSlaveDataSourceFactory.createDataSource(result,....)

BeetlSqlDataSource connectionSource = new BeetlSqlDataSource(){
	@Override
	public Connection getMetaData() {
		return one.getConnection();
	}
}
connectionSource.setMaster(shardingDataSource);
1628
```

connectionSource 唯一不同地方是重载了getMetaData，从而从一个真实的DataSource上获取数据库元信息

> 对于分表应用，比如创建了user001,user002..  User对象必须对应user表，如果未创建user表,这样会报错，可以通过SQLManager.addVirtualTable("user001","user"),告诉BeetlSQL，通过user001来获得“user”的metadata信息

