# 闲大赋的知识星球

**加入知识星球，成为我的星球会员**

<u>为了保证服务质量，知识星球采用收费模式，限定星球人数，6月30号以前优惠价，150元/一年 ，7月以后299元/年 会费</u>，入会地址：[闲大赋的知识星球](https://t.zsxq.com/nYzVN3Z)


提供如下知识服务

* Beetl，BeetlSql，SpringBoot-Plus ，Beetl-BBS的等我所有项目的答疑和讨论，不限于与使用，还有内部实现，设计原理，架构
* 《Springboot 2 精髓》 答疑，Spring Boot 技术使用答疑和讨论
* JavaEE，Spring，互联网等所有我知道的技术的现场答疑和讨论
* 定期会转载或者翻译国内外精华技术文章，精彩代码，新书推荐
* 定期发布我对企业，互联网常用技术，工具的原创介绍，评价和对比
* 大学生到程序员到高级程序员，进阶到架构师的道路的答疑和讨论
* 有机会参与我的开源或者新的开源项目开发，或者新书的的代码例子编写
* 其他任何技术的互动

你身边的架构师，18年开发经验，一流外企呆过，巨无霸电商也混过，不只会扯蛋吹牛，搞过日处理亿级的电信级企业应用，也参与过万台服务器，每秒处理百万级别的电商系统。世界领先beetl开源作者，电子出版社签约畅销书作者，以知行合一，荣辱不惊要求自己，希望能通过我的经验作为你身边的架构师帮助你解惑，成长。




![zsxq2](static/zsxq2.jpg)
![zsxq](static/zsxq.jpg)

# Beetl/BeetlSQL 在线文档

[Beetl/BeetlSQL最新版本获取](https://search.maven.org/search?q=g:com.ibeetl)

```xml
<!-- 在SpringBoot中使用Beetl/BeetlSQL -->
<dependency>
    <groupId>com.ibeetl</groupId>
    <artifactId>beetl-framework-starter</artifactId>
    <version>${最新版本}</version>
</dependency>
```

- [Beetl文档](/beetl/)

  ```xml
  <!-- 单独使用Beetl -->
  <dependency>
      <groupId>com.ibeetl</groupId>
      <artifactId>beetl</artifactId>
      <version>${最新版本}</version>
  </dependency>
  ```

- [BeetlSQL文档](/beetlsql/)

  ```xml
  <!-- 单独使用BeetlSQL -->
  <dependency>
      <groupId>com.ibeetl</groupId>
      <artifactId>beetlsql</artifactId>
      <version>${最新版本}</version>
  </dependency>
  
  ```

  