### 命名转化，表和列名映射 {docsify-ignore}

Beetlsql 默认提供了三种列名和属性名的映射类，推荐使用UnderlinedNameConversion

-   DefaultNameConversion 数据库名和java属性名保持一致，如数据库表User，对应Java类也是User，数据库列是sysytemId,则java属性也是systemId，反之亦然
-   UnderlinedNameConversion 将数据库下划线去掉，首字母大写，如数据库是SYS_USER（oralce数据库的表和属性总是大写的), 则会改成SysUser
-   JPA2NameConversion 支持JPA方式的映射，适合不能用确定的映射关系(2.7.4以前采用JPANameConversion过于简单，已经不用了)
-   自定义命名转化，如果以上3个都不合适,可以自己实现一个命名转化。实现DefaultNameConversion实现方式
-   因为数据库表和列都忽略大小写区别，所以，实现NameConversion也不需要考虑大小写

一般来讲，都建议数据库以下划线来区分单词，因此，使用UnderlinedNameConversion是个很好的选择

```java
	public class DefaultNameConversion extends NameConversion {
	@Override
	public String getTableName(Class<?> c) {
		Table table = (Table)c.getAnnotation(Table.class);
		if(table!=null){
			return table.name();
		}
		return c.getSimpleName();
	}
	
	@Override
	public String getColName(Class<?> c, String attrName) {
		return attrName;
	}
	
	@Override
	public String getPropertyName(Class<?> c, String colName) {
		return colName;
	}

}
```

如果有特殊的表并不符合DefaultNameConversion实现方式，你可以重新实现上面的三个方法

在使用org.beetl.sql.core.JPA2NameConversion作为命令转化规则时，你可以使用以下JPA标签来帮助解析实体类到数据库的转换:

- javax.persistence.Table
- javax.persistence.Column
- javax.persistence.Transient

规则如下：
1 在类名前使用Table注解映射的表名，例如：@Table(name = "PF_TEST")，表示映射的表名是PF_TEST；
2 忽略静态变量以及被@Transient注解的属性；
3 默认属性名与库表的字段名保持一致，如果不一致时，可以使用@Column注解。

```java
@Table(name = "PF_TEST")
public class TestEntity implements Serializable {
	public static String S="SSS";
	private String id;
	@Column(name = "login_name")
	private String loginName;
	private String password;
	private Integer age;
	private Long ttSize;
	private byte[] bigger;
	private String biggerClob;
	@Transient
	private String biggerStr;
	@AssignID
	public String getId() {
	    return id;
	}
	getter setter...
}
```
