### BeetlSQL 数据模型  {docsify-ignore}

BeetlSQL是一个全功能DAO工具，支持的模型也很全面，包括

-   Pojo, 也就是面向对象Java Object。Beetlsql操作将选取Pojo的属性和sql列的交集。额外属性和额外列将忽略.
-   Map/List, 对于一些敏捷开发，可以直接使用Map/List 作为输入输出参数

```java
List<Map<String,Object>> list = sqlManager.select("user.find",Map.class,paras);
```

-   混合模型，推荐使用混合模型。兼具灵活性和更好的维护性。Pojo可以实现Tail（尾巴的意思)，或者继承TailBean，这样查询出的ResultSet 除了按照pojo进行映射外，无法映射的值将按照列表/值保存。如下一个混合模型:

```java
/*混合模型*/
public User extends TailBean{
	private int id ;
	private String name;
	private int roleId;
	/*以下是getter和setter 方法*/
}
```

对于sql语句:

```markdown
selectUser
===
select u.*,r.name r_name from user u left join role r on u.roleId=r.id .....
```

执行查询的时候

```java
List<User> list = sqlManager.select("user.selectUser",User.class,paras);
for(User user:list){
	System.out.println(user.getId());
	System.out.println(user.get("rName"));
}
```

程序可以通过get方法获取到未被映射到pojo的值，也可以在模板里直接 ${user.rName} 显示（对于大多数模板引擎都支持）

另外一种更自由的实现混合模型的方法是在目标Pojo上采用注解@Tail，如果注解不带参数，则默认会调用set(String,Object) 方法来放置额外的查询属性，否则，依据注解的set参数来确定调用方法

```java
@Tail(set="addValue")
public class User  {
	private Integer id ;
	private Integer age ;
	public User addValue(String str,Object ok){
		ext.put(str, ok);
		return this;
	}
```



不仅仅查询结果支持这种混合模型，查询条件也支持。BeetlSql在调用底层SQLScript的时候，传递的参数实际上是Map，包含了sql语句需要的键值对。有一个特殊的键是"_root", 如果Beetlsql未在Map中找到，则从"\_root"中查找，"\_root"可以是个Map，或者是个Pojo。从"\_root" 继续查找

因为这个特性，BeetlSql的参数可以混合Map和Pojo。如下是一些常用例子

~~~java
Map map = new HashMap();
User query = .....
map.put("_root",query);
map.put("maxAge",19);
map.put("minAge",15);
List<User> list = sqlManager.select("user.select",User.class,map);

~~~

sql语句可以使用user的所有属性，“user.select” 如下

~~~sql
select
===

select * from user where name = #name# and (age>=#minAge# and age<=maxAge)

~~~

翻页查询中的参数设置PageQuery.setParas 同样可以使用这种方式，或者直接通过PageQuery API完成

~~~java
PageQuery query = .....
User user = ...
query.setParas(user);
query.setParas("maxAge",19);
query.setParas("minAge",15);
sqlManager.pageQuery("user.query",User.class,query)
 
~~~

实际上，在这种情况下，query对象最后构造的参数就是一个带有"\_root"键值的Map，当然，setParas直接设置一个带“\_root”的Map参数也可以。



> 注意，不要使用TailBean的方法的set 来设置额外参数，尽管可以，但未来考虑不支持。因为TailBean还是放置查询结果。
