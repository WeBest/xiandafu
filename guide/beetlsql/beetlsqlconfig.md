### (重要) 配置beetlsql  {docsify-ignore}

beetlsql 配置文件是 btsql-ext.properties，位于classpath 根目录下，如果没有此文件，beetlsql将使用系统默认配置，如 * 是开发模式，beetlsql每次运行sql都会检测sql文件是否变化，并重新加载 * 字符集，是系统默认的字符集 * 翻页默认总是从1开始，对于oralce数据库来说，翻页起始参数正合适。对于mysql其他数据库来说，beetlsql，翻页参数变成n-1.一般你不需要关心

#### 开发模式和产品模式

beetlsql默认是开发模式，因此修改md的sql文件，不需要重启。但建议线上不要使用开发模式，因为此模式会每次sql调用都会检测md文件是否变化。可以通过修改/btsql-ext.properties ,修改如下属性改为产品模式

```properties
PRODUCT_MODE = true
```



#### NameConversion

数据库字段名与java属性名的映射关系必须配置正确，否则会导致各种问题，如下是一些建议

字段名字是user_id, java属性名是userId, 则使用UnderlinedNameConversion 字段名是userId, java属性名是userId，则使用DefaultNameConversion

如果是其他映射关系，可以考虑自己实现NameConversion接口



#### 模板字符集

默认sql模板文件采用的是系统默认字符集，可以更改配置采用指定的字符集

```properties
CHARSET = UTF-8
```



#### 翻页起始参数是0还是1

默认认为1对应于翻页的第一条记录，如果你习惯mysql 那种0对应于第一条记录，则需要配置OFFSET_START_ZERO，设置为true

```properties
OFFSET_START_ZERO =  true
```

无论是从0开始还是从1开始，都不影响beetlsql根据特定数据库翻译成目标数据库的sql语句，这只是一个约定好的习惯，beetlsql会处理跨数据库翻页的



#### 自定义方法和标签函数

可以在sql模板中使用自定义方法和标签函数，具体请参考beetl使用说明，如下是默认配置

```properties
FN.use = org.beetl.sql.core.engine.UseFunction
FN.globalUse = org.beetl.sql.core.engine.GlobalUseFunction
FN.text = org.beetl.sql.core.engine.TextFunction
FN.join = org.beetl.sql.ext.JoinFunction
FN.isEmpty=org.beetl.sql.ext.EmptyExpressionFunction
FN.page=org.beetl.sql.core.engine.PageQueryFuntion
TAG.trim= org.beetl.sql.core.engine.TrimTag
TAG.pageTag= org.beetl.sql.core.engine.PageQueryTag
```

EmptyExpressionFunction 用在很多地方,如template 类操作,where语句里的条件判断,它 沿用了beetl习惯,对于不存在的变量,或者为null的变量,都返回true,同时如果是字符串,为空字符串也返回true,数组,集合也是这样,有些项目,认为空字符串应该算有值而不应该返回true,你可以参考EmptyExpressionFunction的实现,按照项目语义来定义isEmpty

#### isEmpty 和 isNotEmpty

模板类查询和模板更新，以及Sql语句里的判断都依赖于isEmpty函数判断变量是否存在以及是否为null，2.8.4以前版本对空字符串也认为是空，2.8.4之后版本则仅仅判断对象是否存在以及是否为null

```sql
where 1=1  
@if(!isEmpty(content)){
  and   content = #content#
@}
```

如上代码，如果content存在，且不为null，则进入if代码块

如果想兼容以前的判断的方式，即认为空字符串也是空（不推荐这么用了），则需要在btsql-ext.properties，再次使以前的实现方式

，

```properties
FN.isEmpty = org.beetl.ext.fn.EmptyExpressionFunction
FN.isNotEmpty = org.beetl.ext.fn.IsNotEmptyExpressionFunction
```



你也可以使用beetl的安全输出来表示，比如上面的sql代码，可以用安全输出



```sql
where 1=1  
@if(null!=content!){
  and content = #content
@}
```

如上代码，content ! 是安全表达式，如果不存在或者为null，则为null，然后通过与null比较。
