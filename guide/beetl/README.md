## Beetl 3中文文档 {docsify-ignore}

Beetl作者：李家智(闲大赋)  <[xiandafu@126.com](mailto:xiandafu@126.com)> 

[Beetl单页文档](/beetl/all.md)

微信关注我个人公众号

![gzh](static/gzh.jpg)


### 什么是Beetl {docsify-ignore}



Beetl( 发音同Beetle ) 目前版本是3.0.13,相对于其他java模板引擎，具有功能齐全，语法直观,性能超高，以及编写的模板容易维护等特点。使得开发和维护模板有很好的体验。是新一代的模板引擎。总得来说，它的特性如下：

-   功能完备：作为主流模板引擎，Beetl具有相当多的功能和其他模板引擎不具备的功能。适用于各种应用场景，从对响应速度有很高要求的大网站到功能繁多的CMS管理系统都适合。Beetl本身还具有很多独特功能来完成模板编写和维护，这是其他模板引擎所不具有的。
-   非常简单：类似Javascript语法和习俗，只要半小时就能通过半学半猜完全掌握用法。拒绝其他模板引擎那种非人性化的语法和习俗。同时也能支持html 标签，使得开发CMS系统比较容易
-   超高的性能：Beetl 远超过主流java模板引擎性能(引擎性能5-6倍于FreeMarker，2倍于JSP。参考附录），而且消耗较低的CPU。
-   易于整合：Beetl能很容易的与各种web框架整合，如Spring MVC，JFinal，Struts，Nutz，Jodd，Servlet等。
-   扩展和个性化：Beetl支持自定义方法，格式化函数，虚拟属性，标签，和HTML标签. 同时Beetl也支持自定义占位符和控制语句起始符号也支持使用者完全可以打造适合自己的工具包。
-   模板引擎可以个性化定制
-   可以扩展为脚本引擎，规则引擎，能定制引擎从而实现高级功能。



>   #### 小白如何开始
>
>   -   需要通读基本用法，大部分都是讲解语法，而语法跟js很接近，所以可以快速预览，但Beetl是针对模板设计， 所以像安全输出，标签和html标签，全局变量，临时变量和共享变量，布局技术，以及直接调用java代码等还需要认真读一遍。
>   -   如果从事web开发，还需要阅读web集成里的第一节“web提供的全局变量”，如果web里还使用ajax技术，可以阅读“整合ajax的局部渲染技术”。
>   -   包含有spring,jfinal,jodd,struts 等demo可以作为参考学习用https://git.oschina.net/xiandafu 任何问题，都可以在ibeetl.com 社区上提问。目前答复率是100%，提问需要详细说明自己的期望，出错信息，附上代码或者图片



>   #### 联系作者
>
>   作者：闲.大赋 （李家智）等（参考附录查看代码贡献者)
>
>   QQ技术交流群：219324263(满) 636321496
>
>   邮件：[xiandafu@126.com](mailto:xiandafu@126.com)
>
>   Beetl社区：[bbs.ibeetl.com](http://bbs.ibeetl.com)
>
>   源码主页：[https://github.com/javamonkey/beetl2.0](https://github.com/javamonkey/beetl2.0)
>
>   在线体验和代码分享 [http://ibeetl.com/beetlonline/](http://ibeetl.com/beetlonline/)



> ####  第三方提供的免费视频（以一个博客项目，实战使用）
>
> 项目git地址：https://gitee.com/gavink/beetl-blog
>
> 视频地址：下载下来会更清晰，视频比较长，可使用倍速看
>
> 百度网盘下载: https://pan.baidu.com/s/1LyxAxlKpVXgVjwSXIbzBuA 提取码: 68im
>
> 在线播放地址：bilibili (可以调节清晰度): https://www.bilibili.com/video/av36278644/?p=9
>
> 博客目录：https://my.oschina.net/u/1590490?tab=newest&catalogId=6214598

