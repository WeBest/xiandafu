### SQL 模板基于Beetl实现  {docsify-ignore}

SQL语句可以动态生成，基于Beetl语言，这是因为

-   beetl执行效率高效 ，因此对于基于模板的动态sql语句，采用beetl非常合适
-   beetl 语法简单易用，可以通过半猜半试的方式实现，杜绝myBatis这样难懂难记得语法。BeetlSql学习曲线几乎没有
-   利用beetl可以定制定界符号，完全可以将sql模板定界符好定义为数据库sql注释符号，这样容易在数据库中测试，如下也是sql模板（定义定界符为"--:" 和 null,null是回车意思);

```markdown
selectByCond
===
select * from user where 1=1
--:if(age!=null)
	age=#age#
--:}
```

-   beetl 错误提示非常友好，减少写SQL脚本编写维护时间
-   beetl 能容易与本地类交互（直接访问Java类），能执行一些具体的业务逻辑 ，也可以直接在sql模板中写入模型常量，即使sql重构，也会提前解析报错
-   beetl语句易于扩展，提供各种函数，比如分表逻辑函数，跨数据库的公共函数等

如果不了解beetl，可先自己尝试按照js语法来写sql模板，如果还有疑问，可以查阅官网 [http://ibeetl.com](http://ibeetl.com/)
